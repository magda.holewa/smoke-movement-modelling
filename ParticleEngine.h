//
// Created by magda on 09.12.18.
//

#ifndef SMOKE_PARTICLEENGINE_H
#define SMOKE_PARTICLEENGINE_H


#include <vector>
#include <glm/glm.hpp>
#include "Texture.h"
#include "Shader.h"
#include "Particle.h"
#include "PerlinNoise.h"

class ParticleEngine {
public:
    ParticleEngine();
    void Update(float dt);
    void InitRenderData();
    void SetBoundary(glm::vec3 origin, glm::vec3 position, glm::vec3 size);
    void Draw(const Shader& shader);
    void LoadForceFromFile(const std::string &path);
private:
    std::vector<Particle> particles;
    std::vector<glm::vec3> velocityField;
    std::vector<glm::vec3> velocityPrev;
    std::vector<float> temperatureField;
    std::vector<float> temperaturePrev;
    std::vector<float> pressureField;
    std::vector<float> pressurePrev;
    std::vector<glm::vec3> externalField;
    std::vector<glm::vec3> vort;
    std::vector<glm::vec3> nvec;

    //parameters
    bool enableDiffusion;
    bool enableBuoyancy;
    bool enableExternal;
    bool enableVorticity;
    bool enableGravity;
    float beta;
    float lambda;
    float viscosity;
    float epsilon;
    float externalIntensity;
    float rho;
    float gravity;
    float roomTemperature;
    float smokeTemperature;
    int pressureSolverSteps;
    int velocityDiffusionSolverSteps;
    int temperatureDiffusionSolverSteps;

    bool useFixedTimeStep;
    float fixedTimeStep;

    float particleMaxLifetime;
    float particleMaxAlpha;

    GLuint VAO;
    unsigned int tail = 0;
    glm::vec3 origin;
    glm::vec3 boxPosition;
    glm::vec3 boxSize;
    unsigned int N;
    unsigned int particlesPerFrame;

    std::random_device rd;
    std::mt19937 gen;
    std::uniform_real_distribution<> dis;

    void LoadParametersFromFile(const std::string& path);
    std::string externalForceFile;

    unsigned int particleQueueTail();
    void updateMatrices(float dt);
    void respawn();
    unsigned int getIndex(int i, int j, int k);
    void updateVectorFieldBoundary(std::vector<glm::vec3> &field);
    glm::vec3 interpolateVelocity(glm::vec3 gridPos);

    void updateScalarFieldBoundary(std::vector<float> &vector);

    float interpolateScalar(glm::vec3 vec, const std::vector<float>& vector);
};


#endif //SMOKE_PARTICLEENGINE_H
