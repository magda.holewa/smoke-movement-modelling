//
// Created by magda on 04.12.18.
//

#include "RenderComponent.h"
#include "Object.h"
#include <glm/gtc/matrix_transform.hpp>

RenderComponent::RenderComponent(std::string modelName, glm::vec3 color)
: modelName(std::move(modelName)), color(color){}

glm::mat4 RenderComponent::GetModelMatrix() const {
    glm::mat4 ret(1.0f);
    ret = glm::translate(ret, parent->position);
    return ret;
}

std::string RenderComponent::GetModelName() const {
    return modelName;
}

glm::vec3 RenderComponent::GetColor() const {
    return color;
}

void RenderComponent::SetParent(Object *p) {
    parent = p;
}
