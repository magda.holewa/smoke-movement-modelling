# Smoke Movement Modelling

A project made for university course "Systems Modelling and Simulations". It is a 3D fluid solver, based on Navier-Stokes equations. Also it contains basic setup for game scene in OpenGL. The solver and particle generator, with small modifications, can be easily used in other OpenGL projects and games.

## Build instructions

For Linux, you can use CMakeLists.txt file to build the project. Before that, you need to install libraries: GLM, GLFW, GLAD.
To install GLFW and GLM:

```
sudo apt install libglfw3-dev
sudo apt install libglm-dev
```

GLAD is available at [this website](glad.dav1d.de). Choose **gl version 3.3** and **core** profile. There is also a branch *build_with_glad* with GLAD source.