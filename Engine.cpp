//
// Created by magda on 04.12.18.
//

#include <stdexcept>
#include <algorithm>
#include "Engine.h"
#include "ResourceManager.h"

GLFWwindow* Engine::window;
std::vector<RenderComponent*> Engine::components;
unsigned int Engine::screenWidth;
unsigned int Engine::screenHeight;
bool Engine::keys[1024];
Renderer Engine::renderer;
ParticleEngine Engine::particleEngine;
Camera* Engine::activeCamera;
Scene* Engine::activeScene;
double Engine::lastFrame;
double Engine::deltaTime;
double Engine::lastX;
double Engine::lastY;

void Engine::Initialize(unsigned int scrWidth, unsigned int scrHeight) {
    screenWidth = scrWidth;
    screenHeight = scrHeight;
    lastFrame = 0.0;
    deltaTime = 0.0;
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    window = glfwCreateWindow(screenWidth, screenHeight, "Smoke", nullptr, nullptr);
    if(!window) throw std::runtime_error("Failed to create window");

    glfwMakeContextCurrent(window);

    if(!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
        throw std::runtime_error("Failed to initialize glad");

    /// set callbacks
    glfwSetKeyCallback(window, [](GLFWwindow* window, int key, int scancode, int action, int mode){
        if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
            glfwSetWindowShouldClose(window, GL_TRUE);
        if(action == GLFW_PRESS)
            keys[key] = true;
        else if(action == GLFW_RELEASE)
            keys[key] = false;
    });

    glfwSetCursorPosCallback(window, [](GLFWwindow* window, double xpos, double ypos){
        lastX = xpos;
        lastY = ypos;
        glfwSetCursorPosCallback(window, [](GLFWwindow* window, double xpos, double ypos){
           float xoffset = xpos - lastX;
           float yoffset = lastY - ypos;
           lastX = xpos;
           lastY = ypos;
           activeCamera->ProcessMouseMovement(xoffset, yoffset);
        });
    });
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    /// load resources
    ResourceManager::LoadShader("shaders/default.vert", "shaders/default.frag", "default");
    ResourceManager::LoadShader("shaders/particle.vert", "shaders/particle.frag", "particle");
    ResourceManager::LoadTexture("sprites/whitePuff00.png", "puff");

    renderer.InitRenderData();
    particleEngine.InitRenderData();

    particleEngine.SetBoundary(glm::vec3(0.0f, 1.5f, -3.0f), glm::vec3(0.0f, 2.0f, -3.0f), glm::vec3(1.0f));

    glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
}

void Engine::MainLoop() {
    while(!glfwWindowShouldClose(window)){
        double currentFrame;
        do {
            currentFrame  = glfwGetTime();
            deltaTime = currentFrame - lastFrame;
        }while (deltaTime < 1.0/60);
        lastFrame = currentFrame;

        ProcessKeys();
        UpdateScene(deltaTime);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        Draw();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
}

void Engine::RegisterRenderComponent(RenderComponent *component) {
    components.push_back(component);
}

void Engine::DeleteComponent(RenderComponent *component) {
    components.erase(std::remove_if(std::begin(components), std::end(components),
            [component](RenderComponent* c){return c == component;}), std::end(components));
}

void Engine::Draw() {
    Shader shader = ResourceManager::GetShader("default");
    shader.use();
    shader.setMat4("projection", activeCamera->GetProjectionMatrix());
    shader.setMat4("view", activeCamera->GetViewMatrix());
    shader.setVec3("viewPos", activeCamera->GetPosition());

    renderer.Draw(components, shader);

    shader = ResourceManager::GetShader("particle");
    shader.use();
    shader.setMat4("projection", activeCamera->GetProjectionMatrix());
    shader.setMat4("view", activeCamera->GetViewMatrix());

    particleEngine.Draw(shader);
}

void Engine::ProcessKeys() {
    if(keys[GLFW_KEY_W])
        activeCamera->ProcessKeyboard(Camera::Movement::Forward, deltaTime);
    if(keys[GLFW_KEY_S])
        activeCamera->ProcessKeyboard(Camera::Movement::Backward, deltaTime);
    if(keys[GLFW_KEY_A])
        activeCamera->ProcessKeyboard(Camera::Movement::Left, deltaTime);
    if(keys[GLFW_KEY_D])
        activeCamera->ProcessKeyboard(Camera::Movement::Right, deltaTime);
}

void Engine::Terminate() {
    glfwTerminate();
}

glm::vec2 Engine::GetWindowSize() {
    return glm::vec2(screenWidth, screenHeight);
}

void Engine::SetActiveCamera(Camera *cam) {
    activeCamera = cam;
}

void Engine::SetActiveScene(Scene *scene) {
    activeScene = scene;
}

void Engine::UpdateScene(double dt) {
    activeScene->Update(dt);
    particleEngine.Update(dt);
}

