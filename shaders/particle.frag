#version 330 core

in vec2 texCoords;
out vec4 fragColor;

uniform vec4 color;
uniform sampler2D img;

void main() {
	fragColor = color * texture(img, texCoords);
}
