#version 330 core

in vec3 FragPos;
in vec3 Normal;

out vec4 fragColor;

uniform vec3 viewPos;
uniform vec3 color;

void main(){
    float ambient = 0.2;

    vec3 lightDir = normalize(vec3(0.3, 1.0, 0.6));
    vec3 normal = normalize(Normal);
    if(!gl_FrontFacing)
        normal = -normal;

    float diff = max(dot(lightDir, normal), 0.0);

    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, normal);
    vec3 halfwayDir = normalize(lightDir + viewDir);

    float spec = pow(max(dot(normal, halfwayDir), 0.0), 32.0);
    spec = 0.3 * spec;

    fragColor = vec4(color*(ambient + diff) + spec, 1.0);
}