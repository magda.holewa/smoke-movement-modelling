#version 330 core

layout(location = 0) in vec4 position;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec2 texCoords;

void main() {
    mat4 modelView = view * model;
    modelView[0][0] = model[0][0];
    modelView[0][1] = model[0][1];
    modelView[0][2] = model[0][2];
    modelView[1][0] = model[1][0];
    modelView[1][1] = model[1][1];
    modelView[1][2] = model[1][2];
    modelView[2][0] = model[2][0];
    modelView[2][1] = model[2][1];
    modelView[2][2] = model[2][2];
    modelView[0][0] = model[0][0];
    texCoords = position.zw;
    gl_Position = projection * modelView * vec4(position.xy, 0.0f, 1.0f);
}