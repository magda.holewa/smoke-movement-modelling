//
// Created by magda on 09.12.18.
//

#ifndef SMOKE_PARTICLE_H
#define SMOKE_PARTICLE_H


#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Particle {
public:
    glm::vec3 velocity;
    glm::vec3 position;
    glm::vec4 color;
    float size;
    float lifetime;
    float maxLifetime;

    Particle() : velocity(0.0f, 1.0f, 0.0f), color(1.0f), lifetime(0.0f), maxLifetime(0.0f), size(0.15f) {}

    glm::mat4 GetModelMatrix() {
        glm::mat4 ret(1.0f);
        ret = glm::translate(ret, position);
        ret = glm::scale(ret, glm::vec3(size));
        return ret;
    }
};


#endif //SMOKE_PARTICLE_H
