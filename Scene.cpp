//
// Created by magda on 04.12.18.
//

#include "Scene.h"
#include "RenderComponent.h"
#include "Engine.h"

void Scene::Load() {
    Object* obj = new Object(0, 2, -3);
    RenderComponent* component = new RenderComponent("cube", glm::vec3(0.6f,0.4f,0.0f));
    obj->AddRenderComponent(component);
    Engine::RegisterRenderComponent(component);
    AddObject(obj);
}

void Scene::Clear() {
    for(Object* o : objects)
        delete o;
    objects.clear();
}

void Scene::AddObject(Object *obj) {
    objects.push_back(obj);
}

void Scene::Update(double dt) {
    for(Object* o : objects)
        o->Update(dt);
}
