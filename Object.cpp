//
// Created by magda on 04.12.18.
//

#include "Object.h"

Object::Object(int x, int y, int z)
: position(x,y,z){}

void Object::AddRenderComponent(RenderComponent *c) {
    c->SetParent(this);
    component = c;
}

RenderComponent *Object::GetComponent() {
    return component;
}

void Object::Update(double dt) {
    component->Update(dt);
}
