//
// Created by magda on 04.12.18.
//

#include "Camera.h"
#include <glm/gtc/matrix_transform.hpp>

Camera::Camera(unsigned int width, unsigned int height, glm::vec3 position, glm::vec3 up)
: screenWidth(width), screenHeight(height), zoom(45.0f), position(position), worldUp(up), movementSpeed(3.0f), mouseSensitivity(0.25f){
    yaw = -90.0f;
    pitch = 0.0f;
    UpdateCameraVectors();
}

glm::mat4 Camera::GetViewMatrix() const {
    return glm::lookAt(position, position+front, up);
}

glm::mat4 Camera::GetProjectionMatrix() const {
    return glm::perspective(glm::radians(zoom), (float)screenWidth/(float)screenHeight, 0.1f, 100.0f);
}

void Camera::ProcessKeyboard(Camera::Movement direction, double deltaTime) {
    float velocity = movementSpeed * deltaTime;
    if(direction == Movement::Forward)
        position += front * velocity;
    else if(direction == Movement::Backward)
        position -= front * velocity;
    else if(direction == Movement::Left)
        position -= right * velocity;
    else if(direction == Movement::Right)
        position += right * velocity;
}

void Camera::ProcessMouseMovement(float xoffset, float yoffset) {
    xoffset *= mouseSensitivity;
    yoffset *= mouseSensitivity;

    yaw += xoffset;
    pitch += yoffset;

    if(pitch > 89.0f)
        pitch = 89.0f;
    if(pitch < -89.0f)
        pitch = -89.0f;

    UpdateCameraVectors();
}

glm::vec3 Camera::GetPosition() const {
    return position;
}

void Camera::UpdateCameraVectors() {
    glm::vec3 Front;
    Front.x = (float)(cos(glm::radians(yaw)) * cos(glm::radians(pitch)));
    Front.y = (float)sin(glm::radians(pitch));
    Front.z = (float)(sin(glm::radians(yaw)) * cos(glm::radians(pitch)));
    front = glm::normalize(Front);

    right = glm::normalize(glm::cross(front, worldUp));
    up = glm::normalize(glm::cross(right, front));
}
