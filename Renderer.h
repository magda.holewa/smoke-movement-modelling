//
// Created by magda on 04.12.18.
//

#ifndef SMOKE_RENDERER_H
#define SMOKE_RENDERER_H


#include <vector>
#include <map>
#include "RenderComponent.h"
#include "Shader.h"

class Renderer {
public:
    Renderer();

    void Draw(const std::vector<RenderComponent*>& components, const Shader& shader) const;

    void InitRenderData();
private:

    std::map<std::string, unsigned int> VAOs;

    std::map<std::string, std::vector<float>> vertices;
};


#endif //SMOKE_RENDERER_H
