//
// Created by magda on 04.12.18.
//

#ifndef SMOKE_RESOURCEMANAGER_H
#define SMOKE_RESOURCEMANAGER_H

#include <map>
#include <string>
#include "Shader.h"
#include "Texture.h"

class ResourceManager {
public:
    static void LoadShader(const char* vertexShaderFilePath, const char* fragmentShaderFilePath, std::string name);
    static Shader GetShader(std::string name);

    static void LoadTexture(const char* textureFilePath, std::string name);
    static Texture GetTexture(std::string name);

private:
    static std::map<std::string, Shader> shaders;
    static std::map<std::string, Texture> textures;
};


#endif //SMOKE_RESOURCEMANAGER_H
