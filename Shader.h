//
// Created by magda on 04.12.18.
//

#ifndef SMOKE_SHADER_H
#define SMOKE_SHADER_H

#include <string>
#include <glm/glm.hpp>

class Shader {
public:
    Shader(const char* vertPath, const char* fragPath);

    void use() const;

    void setBool(const std::string& name, bool value) const;
    void setInt(const std::string& name, int value) const;
    void setFloat(const std::string& name, float value) const;
    void setVec2(const std::string& name, float x, float y) const;
    void setVec2(const std::string& name, const glm::vec2& v) const;
    void setVec3(const std::string& name, float x, float y, float z) const;
    void setVec3(const std::string& name, const glm::vec3& v) const;
    void setVec4(const std::string& name, float x, float y, float z, float w) const;
    void setVec4(const std::string& name, const glm::vec4& v) const;
    void setMat4(const std::string& name, const glm::mat4& mat) const;

private:
    void LoadFromFile(const char* vertPath, const char* fragPath);
    void checkErrors(unsigned int shader, std::string type);

    unsigned int id;
};


#endif //SMOKE_SHADER_H
