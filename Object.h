//
// Created by magda on 04.12.18.
//

#ifndef SMOKE_OBJECT_H
#define SMOKE_OBJECT_H

#include <glm/glm.hpp>
#include <vector>
#include "RenderComponent.h"

class Object {
public:
    Object(int x, int y, int z);

    glm::vec3 position;
    //glm::vec3 rotation;
    //glm::vec3 scale;

    void AddRenderComponent(RenderComponent* c);
    RenderComponent* GetComponent();

    void Update(double dt);
private:
    RenderComponent* component;
};


#endif //SMOKE_OBJECT_H
