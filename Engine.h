//
// Created by magda on 04.12.18.
//

#ifndef SMOKE_GFXENGINE_H
#define SMOKE_GFXENGINE_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <vector>
#include "RenderComponent.h"
#include "Renderer.h"
#include "Camera.h"
#include "Scene.h"
#include "ParticleEngine.h"

class Engine {
public:
    static void Initialize(unsigned int scrWidth, unsigned int scrHeight);

    static void MainLoop();

    static void Terminate();

    static void RegisterRenderComponent(RenderComponent* component);
    static void DeleteComponent(RenderComponent* component);

    static glm::vec2 GetWindowSize();

    static void SetActiveCamera(Camera* cam);
    static void SetActiveScene(Scene* scene);

private:
    static void ProcessKeys();
    static void Draw();
    static void UpdateScene(double dt);

    static Camera* activeCamera;
    static Scene* activeScene;

    static Renderer renderer;
    static ParticleEngine particleEngine;

    static std::vector<RenderComponent*> components;

    static GLFWwindow* window;

    static unsigned int screenWidth;
    static unsigned int screenHeight;
    static bool keys[1024];
    static double lastFrame, deltaTime;
    static double lastX, lastY;
};


#endif //SMOKE_GFXENGINE_H
