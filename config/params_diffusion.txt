particlesPerFrame 20
N 26
enableDiffusion 1
enableBuoyancy 0
enableExternal 1
enableVorticity 0
enableGravity 0

useFixedTimeStep 0
fixedTimeStep 0.16

rho 50.0
beta 0.8
lambda 0.1
viscosity 0.1
epsilon 10.6
externalIntensity 1.5
gravity 2.5
pressureSolverSteps 5
temperatureDiffusionSolverSteps 5
velocityDiffusionSolverSteps 5
roomTemperature 0.2
smokeTemperature 3.5

particleMaxLifetime 30.0
particleMaxAlpha 0.01

externalFile config/upstream.txt
