#include <iostream>
#include "Engine.h"
#include "Scene.h"
#include "Camera.h"

const unsigned int winWidth = 1024, winHeight = 768;

int main() {
    Engine::Initialize(winWidth, winHeight);

    Scene scene;
    scene.Load();

    Camera camera(winWidth, winHeight, glm::vec3(0.0f,2.0f,0.0f));

    Engine::SetActiveCamera(&camera);
    Engine::SetActiveScene(&scene);

    Engine::MainLoop();

    scene.Clear();
    Engine::Terminate();

    return 0;
}