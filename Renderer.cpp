//
// Created by magda on 04.12.18.
//

#include <glad/glad.h>
#include "Renderer.h"

Renderer::Renderer() {
}

void Renderer::Draw(const std::vector<RenderComponent *> &components, const Shader& shader) const {
    shader.use();
    for(RenderComponent* c : components){
        shader.setMat4("model", c->GetModelMatrix());
        shader.setVec3("color", c->GetColor());

        if (c->GetModelName() == "cube") {
            glEnable(GL_CULL_FACE);
            glCullFace(GL_FRONT);
        }

        glBindVertexArray(VAOs.at(c->GetModelName()));
        glDrawArrays(GL_TRIANGLES, 0, vertices.at(c->GetModelName()).size());
        glBindVertexArray(0);
        glDisable(GL_CULL_FACE);
    }
}

void Renderer::InitRenderData() {

    vertices["cube"] = {
            -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f,
            0.5f,  0.5f, -0.5f, 0.0f, 0.0f, -1.0f,
            0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f,
            0.5f,  0.5f, -0.5f, 0.0f, 0.0f, -1.0f,
            -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f,
            -0.5f,  0.5f, -0.5f, 0.0f, 0.0f, -1.0f,

            -0.5f, -0.5f,  0.5f, 0.0f, 0.0f, 1.0f,
            0.5f, -0.5f,  0.5f, 0.0f, 0.0f, 1.0f,
            0.5f,  0.5f,  0.5f, 0.0f, 0.0f, 1.0f,
            0.5f,  0.5f,  0.5f, 0.0f, 0.0f, 1.0f,
            -0.5f,  0.5f,  0.5f, 0.0f, 0.0f, 1.0f,
            -0.5f, -0.5f,  0.5f, 0.0f, 0.0f, 1.0f,

            -0.5f,  0.5f,  0.5f, -1.0f, 0.0f, 0.0f,
            -0.5f,  0.5f, -0.5f, -1.0f, 0.0f, 0.0f,
            -0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f,
            -0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f,
            -0.5f, -0.5f,  0.5f, -1.0f, 0.0f, 0.0f,
            -0.5f,  0.5f,  0.5f, -1.0f, 0.0f, 0.0f,

            0.5f,  0.5f,  0.5f, 1.0f, 0.0f, 0.0f,
            0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f,
            0.5f,  0.5f, -0.5f, 1.0f, 0.0f, 0.0f,
            0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f,
            0.5f,  0.5f,  0.5f, 1.0f, 0.0f, 0.0f,
            0.5f, -0.5f,  0.5f, 1.0f, 0.0f, 0.0f,

            -0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f,
            0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f,
            0.5f, -0.5f,  0.5f, 0.0f, -1.0f, 0.0f,
            0.5f, -0.5f,  0.5f, 0.0f, -1.0f, 0.0f,
            -0.5f, -0.5f,  0.5f, 0.0f, -1.0f, 0.0f,
            -0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f,

            -0.5f,  0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
            0.5f,  0.5f,  0.5f, 0.0f, 1.0f, 0.0f,
            0.5f,  0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
            0.5f,  0.5f,  0.5f, 0.0f, 1.0f, 0.0f,
            -0.5f,  0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
            -0.5f,  0.5f,  0.5f, 0.0f, 1.0f, 0.0f
    };
    vertices["plane"]={
         -0.5f, -0.5f,  0.0f,    0.0f, 0.0f, 1.0f,
          0.5f,  0.5f,  0.0f,    0.0f, 0.0f, 1.0f,
          0.5f, -0.5f,  0.0f,    0.0f, 0.0f, 1.0f,
          0.5f,  0.5f,  0.0f,    0.0f, 0.0f, 1.0f,
         -0.5f, -0.5f,  0.0f,    0.0f, 0.0f, 1.0f,
         -0.5f,  0.5f,  0.0f,    0.0f, 0.0f, 1.0f
    };

    unsigned int CubeVBO;
    glGenVertexArrays(1, &VAOs["cube"]);
    glGenBuffers(1, &CubeVBO);

    glBindVertexArray(VAOs["cube"]);
    glBindBuffer(GL_ARRAY_BUFFER, CubeVBO);

    glBufferData(GL_ARRAY_BUFFER, vertices.at("cube").size() * sizeof(float), &vertices.at("cube")[0], GL_STATIC_DRAW);
    glVertexAttribPointer(0,3,GL_FLOAT, GL_FALSE, 6*sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1,3,GL_FLOAT, GL_FALSE, 6*sizeof(float), (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

    glBindVertexArray(0);



    unsigned int PlaneVBO;
    glGenVertexArrays(1, &VAOs["plane"]);
    glGenBuffers(1, &PlaneVBO);

    glBindVertexArray(VAOs["plane"]);
    glBindBuffer(GL_ARRAY_BUFFER, PlaneVBO);

    glBufferData(GL_ARRAY_BUFFER, vertices.at("plane").size() * sizeof(float), &vertices.at("plane")[0], GL_STATIC_DRAW);
    glVertexAttribPointer(0,3,GL_FLOAT, GL_FALSE, 6*sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1,3,GL_FLOAT, GL_FALSE, 6*sizeof(float), (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

    glBindVertexArray(0);
}
