//
// Created by magda on 04.12.18.
//

#ifndef SMOKE_RENDERCOMPONENT_H
#define SMOKE_RENDERCOMPONENT_H

#include <string>
#include <glm/glm.hpp>

class Object;

class RenderComponent {
public:
    RenderComponent(std::string modelName, glm::vec3 color);

    void Update(double dt) {}

    glm::mat4 GetModelMatrix() const;

    std::string GetModelName() const;
    glm::vec3 GetColor() const;

    void SetParent(Object* p);
private:
    std::string modelName;
    glm::vec3 color;
    Object* parent;
};


#endif //SMOKE_RENDERCOMPONENT_H
