//
// Created by magda on 04.12.18.
//

#ifndef SMOKE_SCENE_H
#define SMOKE_SCENE_H


#include <vector>
#include "Object.h"

class Scene {
public:
    void Load();
    void Update(double dt);
    void Clear();
private:
    void AddObject(Object* obj);
    std::vector<Object*> objects;
};


#endif //SMOKE_SCENE_H
