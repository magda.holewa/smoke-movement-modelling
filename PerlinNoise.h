//
// Created by magda on 27.12.18.
//
// Based on the original Perlin Noise code in Java
// Written by Ken Perlin
//

#ifndef SMOKE_PERLINNOISE_H
#define SMOKE_PERLINNOISE_H

#include <cmath>
#include <vector>
#include <algorithm>
#include <numeric>
#include <iostream>
#include <random>

class PerlinNoise {
public:
    double noise(double x, double y, double z);
    PerlinNoise();
private:
    double fade(double t);
    double lerp(double t, double a, double b);
    double grad(int hash, double x, double y, double z);
    std::vector<int> p;
    std::vector<int> permutation;
};

#endif //SMOKE_PERLINNOISE_H
