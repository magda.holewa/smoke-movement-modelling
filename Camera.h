//
// Created by magda on 04.12.18.
//

#ifndef SMOKE_CAMERA_H
#define SMOKE_CAMERA_H

#include <glm/glm.hpp>

class Camera {
public:
    enum class Movement{
        Forward, Backward, Left, Right
    };

    Camera(unsigned int width, unsigned int height, glm::vec3 position, glm::vec3 up = glm::vec3(0.0f,1.0f,0.0f));

    glm::mat4 GetViewMatrix() const;
    glm::mat4 GetProjectionMatrix() const;

    void ProcessKeyboard(Movement direction, double deltaTime);

    void ProcessMouseMovement(float xoffset, float yoffset);

    glm::vec3 GetPosition() const;
private:
    void UpdateCameraVectors();

    glm::vec3 position;
    glm::vec3 front;
    glm::vec3 up;
    glm::vec3 right;
    glm::vec3 worldUp;

    float yaw;
    float pitch;

    float movementSpeed;
    float mouseSensitivity;

    const float zoom;
    const unsigned int screenWidth;
    const unsigned int screenHeight;
};


#endif //SMOKE_CAMERA_H
