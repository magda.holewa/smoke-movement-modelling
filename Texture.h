//
// Created by magda on 03.12.18.
//

#ifndef SMOKEPARTICLES_TEXTURE_H
#define SMOKEPARTICLES_TEXTURE_H


#include <glad/glad.h>

class Texture {
public:
    Texture(const char* filePath);

    void Bind() const;

    int GetWidth() const;
    int GetHeight() const;
    int GetNumberOfChannels() const;
private:
    void LoadFromFile(const char* filePath);

    unsigned int id;
    int width, height, nrChannels;
};

#endif //SMOKEPARTICLES_TEXTURE_H
