//
// Created by magda on 09.12.18.
//

#include <iostream>
#include <vector>
#include <fstream>
#include "ParticleEngine.h"
#include "ResourceManager.h"
#include "PerlinNoise.h"

ParticleEngine::ParticleEngine() {
    externalForceFile = "";
    LoadParametersFromFile("config/params.txt");

    unsigned int limit = 100000;
    for (int i=0; i<limit; ++i) {
        particles.emplace_back();
    }

    gen = std::mt19937(rd());
    dis = std::uniform_real_distribution<>(-0.1f, 0.1f);

    velocityField = std::vector<glm::vec3>((N + 2) * (N+2) * (N+2));
    velocityPrev = std::vector<glm::vec3>((N + 2) * (N+2) * (N+2));
    PerlinNoise perlinNoiseX;
    PerlinNoise perlinNoiseY;
    PerlinNoise perlinNoiseZ;

    externalField = std::vector<glm::vec3>((N + 2) * (N+2) * (N+2));

    if (!externalForceFile.empty()) LoadForceFromFile(externalForceFile);
    for (int i = 0; i < N+2; ++i) {
        for (int j = 0; j < N+2; ++j) {
            for (int k = 0; k < N+2; ++k) {
                float valueX = (float) perlinNoiseX.noise((double)(i+1) / N,(double)(j+1) / N,(double)(k+1) / N) * 0.01f;
                float valueY = (float) perlinNoiseY.noise((double)(i+1) / N,(double)(j+1) / N,(double)(k+1) / N) * 0.01f;
                float valueZ = (float) perlinNoiseZ.noise((double)(i+1) / N,(double)(j+1) / N,(double)(k+1) / N) * 0.01f;
                velocityField[getIndex(i,j,k)] = glm::vec3(valueX,valueY,valueZ);
            }
        }
    }
    updateVectorFieldBoundary(velocityField);
    pressureField = std::vector<float>((N + 2) * (N+2) * (N+2));
    pressurePrev = std::vector<float>((N + 2) * (N+2) * (N+2));

    temperatureField = std::vector<float>((N + 2) * (N+2) * (N+2));
    for (auto& x : temperatureField) x = roomTemperature;
    temperaturePrev = std::vector<float>((N + 2) * (N+2) * (N+2));
    for (auto& x : temperaturePrev) x = roomTemperature;

    vort = std::vector<glm::vec3>((N + 2) * (N+2) * (N+2));
    nvec = std::vector<glm::vec3>((N + 2) * (N+2) * (N+2));
}

void ParticleEngine::LoadForceFromFile(const std::string &path) {
    std::ifstream f(path);
    for (int i = 1; i <= N; ++i) {
        for (int j = 1; j <= N; ++j) {
            for (int k = 1; k<= N; ++k) {
                f >> externalField[getIndex(i,j,k)].x;
            }
        }
    }

    for (int i = 1; i <= N; ++i) {
        for (int j = 1; j <= N; ++j) {
            for (int k = 1; k<= N; ++k) {
                f >> externalField[getIndex(i,j,k)].y;
            }
        }
    }

    for (int i = 1; i <= N; ++i) {
        for (int j = 1; j <= N; ++j) {
            for (int k = 1; k<= N; ++k) {
                f >> externalField[getIndex(i,j,k)].z;
            }
        }
    }
}

void ParticleEngine::LoadParametersFromFile(const std::string &path) {
    std::ifstream file(path);
    std::string tmpname;
    file >> tmpname; file >> particlesPerFrame;
    file >> tmpname; file >> N;
    file >> tmpname; file >> enableDiffusion;
    file >> tmpname; file >> enableBuoyancy;
    file >> tmpname; file >> enableExternal;
    file >> tmpname; file >> enableVorticity;
    file >> tmpname; file >> enableGravity;

    file >> tmpname; file >> useFixedTimeStep;
    file >> tmpname; file >> fixedTimeStep;

    file >> tmpname; file >> rho;
    file >> tmpname; file >> beta;
    file >> tmpname; file >> lambda;
    file >> tmpname; file >> viscosity;
    file >> tmpname; file >> epsilon;
    file >> tmpname; file >> externalIntensity;
    file >> tmpname; file >> gravity;
    file >> tmpname; file >> pressureSolverSteps;
    file >> tmpname; file >> temperatureDiffusionSolverSteps;
    file >> tmpname; file >> velocityDiffusionSolverSteps;

    file >> tmpname; file >> roomTemperature;
    file >> tmpname; file >> smokeTemperature;

    file >> tmpname; file >> particleMaxLifetime;
    file >> tmpname; file >> particleMaxAlpha;

    if (enableExternal) {
        file >> tmpname; file >> externalForceFile;
    }

    file.close();
}

void ParticleEngine::Update(float dt) {
    if (useFixedTimeStep)
        dt = fixedTimeStep;
    respawn();
    updateMatrices(dt);
    for (auto& p : particles) {
        if (p.lifetime > 0.0f) {
            p.color.a = p.lifetime * particleMaxAlpha / p.maxLifetime;
            p.lifetime -= dt;
            p.position += p.velocity * (dt);
            glm::vec3 dist = p.position - (boxPosition - glm::vec3(0.5f));
            if (dist.x < 0.0f) {
                p.position.x = boxPosition.x - 0.5f + 0.5f * p.size;
            }
            else if (dist.x > 1.0f) {
                p.position.x = boxPosition.x + 0.5f - 0.5f * p.size;
            }
            if (dist.y < 0.0f) {
                p.position.y = boxPosition.y - 0.5f + 0.5f * p.size;
            }
            else if (dist.y > 1.0f) {
                p.position.y = boxPosition.y + 0.5f - 0.5f * p.size;
            }
            if (dist.z < 0.0f) {
                p.position.z = boxPosition.z - 0.5f + 0.5f * p.size;
            }
            else if (dist.z > 1.0f) {
                p.position.z = boxPosition.z +0.5f - 0.5f * p.size;
            }
            dist = p.position - (boxPosition - glm::vec3(0.5f));

            if (dist.x >= 0 && dist.x <= 1.0f && dist.y >= 0 && dist.y <= 1.0f && dist.z >= 0 && dist.z <= 1.0f) {
                dist *= (float) N;
                dist += glm::vec3(1.0f);
                glm::vec3 pos = glm::round(dist) - glm::vec3(1.0f);
                if (pos.x > N) pos.x = N;
                if (pos.y > N) pos.y = N;
                if (pos.z > N) pos.z = N;
                if (pos.x < 0) pos.x = 0;
                if (pos.y < 0) pos.y = 0;
                if (pos.z < 0) pos.z = 0;
                p.velocity = interpolateVelocity(pos);
            }
            else {
                p.color.r = 0.1;
                if (dist.x < 0 || dist.x > 1.0f) {
                    p.velocity.x *= -1.0f;
                }
                if (dist.y < 0 || dist.y > 1.0f) {
                    p.velocity.y *= -1.0f;
                }
                if (dist.z < 0 || dist.z > 1.0f) {
                    p.velocity.z *= -1.0f;
                }
            }
        }
    }
}

void ParticleEngine::InitRenderData() {
    GLuint VBO;
    GLfloat particle_quad[] = {
            -0.5f, 0.5f, 0.0f, 1.0f,
            0.5f, -0.5f, 1.0f, 0.0f,
            -0.5f, -0.5f, 0.0f, 0.0f,

            -0.5f, 0.5f, 0.0f, 1.0f,
            0.5f, 0.5f, 1.0f, 1.0f,
            0.5f, -0.5f, 1.0f, 0.0f
    };
    glGenVertexArrays(1, &this->VAO);
    glGenBuffers(1, &VBO);
    glBindVertexArray(this->VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(particle_quad), particle_quad, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
    glBindVertexArray(0);
}

void ParticleEngine::SetBoundary(glm::vec3 origin, glm::vec3 position, glm::vec3 size) {
    this->origin = origin;
    boxPosition = position;
    boxSize = size;
}

void ParticleEngine::Draw(const Shader& shader) {
    glDisable(GL_DEPTH_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    shader.use();
    auto texture = ResourceManager::GetTexture("puff");
    glActiveTexture(GL_TEXTURE0);
    texture.Bind();
    shader.setInt("img", 0);
    glBindVertexArray(VAO);
    for (Particle& p : particles) {
        if (p.lifetime > 0.0f) {
            shader.setMat4("model", p.GetModelMatrix());
            shader.setVec4("color", p.color);
            glDrawArrays(GL_TRIANGLES, 0, 6);
        }
    }
    glBindVertexArray(0);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_DEPTH_TEST);
}

unsigned int ParticleEngine::particleQueueTail() {
    /// helper method: pierwsza gotowa do inicjalizacji cząstka
    for (unsigned int i = tail; i < particles.size(); ++i) {
        if (particles[i].lifetime <= 0.0f) {
            tail = i;
            return i;
        }
    }
    for (unsigned int i = 0; i < tail; ++i) {
        if (particles[i].lifetime <= 0.0f) {
            tail = i;
            return i;
        }
    }
    tail = 0;
    return 0;
}

void ParticleEngine::respawn() {
    /// umieszcza nową cząstkę w losowej pozycji w pobliżu origin
    for (int i = 0; i < particlesPerFrame; ++i) {
        Particle* p = &particles[particleQueueTail()];
        p->lifetime = particleMaxLifetime;
        p->maxLifetime = p->lifetime;
        p->position = origin;
        p->position.x += (float)dis(gen);
        //p->position.y += (float)dis(gen);
        p->position.z += (float)dis(gen);
        p->position.y += 1.0f / N;
        p->velocity = glm::vec3(0.0f);
        p->color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
    }
}

void ParticleEngine::updateMatrices(float dt) {
    auto dv = std::vector<glm::vec3>((N + 2) * (N+2) * (N+2));

    float h = 1.0f / N;
    float dt0 = dt * N;
    float tmean = 0.0f;
    glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);

    ///wirowość
    if (enableVorticity) {
        for (int i = 1; i <= N; ++i) {
            for (int j = 1; j <= N; ++j) {
                for (int k = 1; k <= N; ++k) {
                    glm::vec3 value;
                    value.x = velocityField[getIndex(i,j+1,k)].z - velocityField[getIndex(i,j-1,k)].z
                              - velocityField[getIndex(i,j,k+1)].y + velocityField[getIndex(i,j,k-1)].y;
                    value.y = velocityField[getIndex(i,j,k+1)].x - velocityField[getIndex(i,j,k-1)].x
                              - velocityField[getIndex(i+1,j,k)].z + velocityField[getIndex(i+1,j,k)].z;
                    value.z = velocityField[getIndex(i+1,j,k)].y - velocityField[getIndex(i-1,j,k)].y
                              - velocityField[getIndex(i,j+1,k)].x + velocityField[getIndex(i,j-1,k)].x;
                    vort[getIndex(i,j,k)] = value / (2*h);
                }
            }
        }

        for (int i = 1; i <= N; ++i) {
            for (int j = 1; j <= N; ++j) {
                for (int k = 1; k <= N; ++k) {
                    glm::vec3 value;
                    value.x = glm::length(vort[getIndex(i+1,j,k)]) - glm::length(vort[getIndex(i-1,j,k)]);
                    value.y = glm::length(vort[getIndex(i,j+1,k)]) - glm::length(vort[getIndex(i,j-1,k)]);
                    value.z = glm::length(vort[getIndex(i,j,k+1)]) - glm::length(vort[getIndex(i,j,k-1)]);
                    nvec[getIndex(i,j,k)] = glm::normalize(value / (2*h));
                }
            }
        }

        updateVectorFieldBoundary(vort);
    }

    ///temperatura - dodawanie ciepła ze źródła
    if (enableBuoyancy) {
        glm::vec3 originCoords = origin - (boxPosition - glm::vec3(0.5f));
        originCoords *= (float) N;
        originCoords += 0.5f * boxSize / (float) N;

        temperatureField[getIndex(N/2, 1, N/2)] = smokeTemperature;
        temperatureField[getIndex(N/2 + 1, 1, N/2 + 1)] = smokeTemperature;
        temperatureField[getIndex(N/2, 1, N/2 + 1)] = smokeTemperature;
        temperatureField[getIndex(N/2 + 1, 1, N/2)] = smokeTemperature;

        temperatureField.swap(temperaturePrev);

        /// adwekcja
        for (int i = 1; i<=N; ++i) {
            for (int j = 1; j<=N; ++j) {
                for (int k =1; k<=N; ++k) {
                    float x = i - dt0 * velocityField[getIndex(i,j,k)].x;
                    float y = j - dt0 * velocityField[getIndex(i,j,k)].y;
                    float z = k - dt0 * velocityField[getIndex(i,j,k)].z;
                    if (x < 0.5f) x = 0.5f;
                    if (x > N + 0.5f) x = N + 0.5f;
                    if (y < 0.5f) y = 0.5f;
                    if (y > N + 0.5f) y = N + 0.5f;
                    if (z < 0.5f) z = 0.5f;
                    if (z > N + 0.5f) z = N + 0.5f;
                    temperatureField[getIndex(i,j,k)] = interpolateScalar(glm::vec3(x, y, z), temperaturePrev);
                }
            }
        }
        //updateScalarFieldBoundary(temperatureField);
        temperaturePrev.swap(temperatureField);

        /// dyfuzja temperatury
        float al = dt * lambda * N * N;

        for (int c = 0; c < temperatureDiffusionSolverSteps; c++) {
            for (int i = 1; i <= N; ++i) {
                for (int j = 1; j<=N; ++j) {
                    for (int k = 1; k<=N; ++k) {
                        temperatureField[getIndex(i,j,k)] = (temperaturePrev[getIndex(i,j,k)] + al * (temperatureField[getIndex(i+1,j,k)]
                                                            + temperatureField[getIndex(i-1,j,k)] + temperatureField[getIndex(i,j+1,k)]
                                                            + temperatureField[getIndex(i,j-1,k)] + temperatureField[getIndex(i,j,k+1)]
                                                            + temperatureField[getIndex(i,j,k-1)])) / (1 + 6*al);
                    }
                }
            }
            //updateScalarFieldBoundary(temperatureField);
            //temperaturePrev.swap(temperatureField);
        }
        //temperaturePrev.swap(temperatureField);
        //updateScalarFieldBoundary(temperatureField);

        /// obliczanie średniej temperatury - potrzebne przy liczeniu siły
        for (int i =1; i<=N; ++i) {
            for (int j = 1; j<= N; ++j) {
                for (int k =1; k<=N; ++k) {
                    tmean += temperatureField[getIndex(i,j,k)];
                }
            }
        }
        tmean /= (N*N*N);
    }

    /// siły zewnętrzne
    for (int i =1; i <= N ; ++i) {
        for (int j = 1; j <= N; ++j) {
            for (int k = 1; k <= N; ++k) {
                if (enableGravity) velocityField[getIndex(i,j,k)].y -= gravity * dt;
                if (enableVorticity) velocityField[getIndex(i,j,k)] += epsilon * h * glm::cross(nvec[getIndex(i,j,k)], vort[getIndex(i,j,k)]) * dt;
                if (enableExternal) velocityField[getIndex(i,j,k)] += externalField[getIndex(i,j,k)] * externalIntensity * dt;
                if (enableBuoyancy) velocityField[getIndex(i,j,k)] += beta * up * (temperatureField[getIndex(i,j,k)] - tmean);
            }
        }
    }

    /// adwekcja prędkości
    dt0 = dt * N;
    for (int i = 1; i<=N; ++i) {
        for (int j = 1; j<=N; ++j) {
            for (int k =1; k<=N; ++k) {
                float x = i - dt0 * velocityField[getIndex(i,j,k)].x;
                float y = j - dt0 * velocityField[getIndex(i,j,k)].y;
                float z = k - dt0 * velocityField[getIndex(i,j,k)].z;
                if (x < 0.5f) x = 0.5f;
                if (x > N + 0.5f) x = N + 0.5f;
                if (y < 0.5f) y = 0.5f;
                if (y > N + 0.5f) y = N + 0.5f;
                if (z < 0.5f) z = 0.5f;
                if (z > N + 0.5f) z = N + 0.5f;
                velocityPrev[getIndex(i,j,k)] = interpolateVelocity(glm::vec3(x, y, z));
            }
        }
    }

    velocityField.swap(velocityPrev);
    updateVectorFieldBoundary(velocityField);

    /// obliczanie wpływu dyfuzji
    if (enableDiffusion) {
        velocityField.swap(velocityPrev);
        float a = dt * viscosity * N * N;
        for (int i = 0; i <= N+1; ++i) {
            for (int j = 0; j<=N+1; ++j) {
                for (int k = 0; k<=N+1; ++k) {
                    dv[getIndex(i,j,k)] = velocityPrev[getIndex(i,j,k)];
                }
            }
        }
        dv.swap(velocityField);
        for (int c = 0; c < velocityDiffusionSolverSteps; c++) {
            for (int i = 1; i <= N; ++i) {
                for (int j = 1; j<=N; ++j) {
                    for (int k = 1; k<=N; ++k) {
                        dv[getIndex(i,j,k)] = (velocityPrev[getIndex(i,j,k)] + a * (dv[getIndex(i+1,j,k)] + dv[getIndex(i-1,j,k)] + dv[getIndex(i,j+1,k)]
                                                                                    + dv[getIndex(i,j-1,k)] + dv[getIndex(i,j,k+1)] + dv[getIndex(i,j,k-1)])) / (1 + 6*a);
                    }
                }
            }
        }
        velocityField.swap(dv);
        updateVectorFieldBoundary(velocityField);
    }

    /// obliczanie ciśnienia
    auto delta = std::vector<float>((N+2)*(N+2)*(N+2));

    for (int i = 1; i <= N; ++i) {
        for (int j = 1; j <= N; ++j) {
            for (int k = 1; k <= N; ++k) {
                delta[getIndex(i,j,k)] = 0.5f / h * (velocityField[getIndex(i+1,j,k)].x - velocityField[getIndex(i-1,j,k)].x
                        + velocityField[getIndex(i,j+1,k)].y - velocityField[getIndex(i,j-1,k)].y
                        + velocityField[getIndex(i,j,k+1)].z - velocityField[getIndex(i,j,k-1)].z);
            }
        }
    }

    for (auto& x : pressureField) x = 0.0f;

    for (int c = 1; c < pressureSolverSteps; c++) {
        pressurePrev.swap(pressureField);
        for (int i = 1; i <= N; ++i) {
            for (int j = 1; j <= N; ++j) {
                for (int k = 1; k <= N; ++k) {
                    pressureField[getIndex(i,j,k)] = (pressurePrev[getIndex(i+1,j,k)] + pressurePrev[getIndex(i-1,j,k)]
                            + pressurePrev[getIndex(i,j+1,k)] + pressurePrev[getIndex(i,j-1,k)]
                            + pressurePrev[getIndex(i,j,k+1)] + pressurePrev[getIndex(i,j,k-1)]
                            - rho * h * h * delta[getIndex(i,j,k)]) / 6.0f;
                }
            }
        }
    }
    updateScalarFieldBoundary(pressureField);

    /// obliczanie wpływu ciśnienia na prędkość
    for (int i = 1; i <= N; ++i) {
        for (int j = 1; j <= N; ++j) {
            for (int k = 1; k <= N; ++k) {
                velocityField[getIndex(i,j,k)].x -= (pressureField[getIndex(i+1,j,k)] - pressureField[getIndex(i-1,j,k)]) / (2 * rho * h);
                velocityField[getIndex(i,j,k)].y -= (pressureField[getIndex(i,j+1,k)] - pressureField[getIndex(i,j-1,k)]) / (2 * rho * h);
                velocityField[getIndex(i,j,k)].z -= (pressureField[getIndex(i,j,k+1)] - pressureField[getIndex(i,j,k-1)]) / (2 * rho * h);
            }
        }
    }
    updateVectorFieldBoundary(velocityField);
}

unsigned int ParticleEngine::getIndex(int i, int j, int k) {
    return (i * (N+2) + j) * (N+2) + k;
}

void ParticleEngine::updateVectorFieldBoundary(std::vector<glm::vec3> &field) {

    for (int i = 1; i <= N; ++i) {
        for (int j = 1; j <= N; ++j) {
            field[getIndex(0,i,j)] = field[getIndex(1,i,j)] * glm::vec3(-1.0f,1.0f,1.0f);
            field[getIndex(N+1,i,j)] = field[getIndex(N,i,j)] * glm::vec3(-1.0f,1.0f,1.0f);
            field[getIndex(i,0,j)] = field[getIndex(i,1,j)] * glm::vec3(1.0f,-1.0f,1.0f);
            field[getIndex(i,N+1,j)] = field[getIndex(i,N,j)] * glm::vec3(1.0f,-1.0f,1.0f);
            field[getIndex(i,j,0)] = field[getIndex(i,j,1)] * glm::vec3(1.0f,1.0f,-1.0f);
            field[getIndex(i,j,N+1)] = field[getIndex(i,j,N)] * glm::vec3(1.0f,1.0f,-1.0f);
        }
    }

    for (int i =1; i<=N; ++i) {
        field[getIndex(0,0,i)] = 0.5f * (field[getIndex(0,1,i)] + field[getIndex(1,0,i)]);
        field[getIndex(0,i,0)] = 0.5f * (field[getIndex(0,i,1)] + field[getIndex(1,i,0)]);
        field[getIndex(i,0,0)] = 0.5f * (field[getIndex(i,1,0)] + field[getIndex(i,0,1)]);
        field[getIndex(N+1,N+1,i)] = 0.5f * (field[getIndex(N+1,N,i)] + field[getIndex(N,N+1,i)]);
        field[getIndex(N+1,i,N+1)] = 0.5f * (field[getIndex(N+1,i,N)] + field[getIndex(N,i,N+1)]);
        field[getIndex(i,N+1,N+1)] = 0.5f * (field[getIndex(i,N,N+1)] + field[getIndex(i,N+1,N)]);
        field[getIndex(N+1,0,i)] = 0.5f * (field[getIndex(N+1,1,i)] + field[getIndex(N,0,i)]);
        field[getIndex(0,N+1,i)] = 0.5f * (field[getIndex(0,N,i)] + field[getIndex(1,N+1,i)]);
        field[getIndex(N+1,i,0)] = 0.5f * (field[getIndex(N+1,i,1)] + field[getIndex(N,i,0)]);
        field[getIndex(0,i,N+1)] = 0.5f * (field[getIndex(0,i,N)] + field[getIndex(1,i,N+1)]);
        field[getIndex(i,N+1,0)] = 0.5f * (field[getIndex(i,N,0)] + field[getIndex(i,N+1,1)]);
        field[getIndex(i,0,N+1)] = 0.5f * (field[getIndex(i,1,N+1)] + field[getIndex(i,0,N)]);
    }

    field[getIndex(0,0,0)] = 1.0f/3.0f * (field[getIndex(0,0,1)] + field[getIndex(0,1,0)] + field[getIndex(1,0,0)]);
    field[getIndex(0,0,N+1)] = 1.0f/3.0f * (field[getIndex(0,0,N)] + field[getIndex(0,1,N+1)] + field[getIndex(1,0,N+1)]);
    field[getIndex(0,N+1,0)] = 1.0f/3.0f * (field[getIndex(0,N+1,1)] + field[getIndex(0,N,0)] + field[getIndex(1,N+1,0)]);
    field[getIndex(N+1,0,0)] = 1.0f/3.0f * (field[getIndex(N+1,0,1)] + field[getIndex(N+1,1,0)] + field[getIndex(N,0,0)]);
    field[getIndex(N+1,0,N+1)] = 1.0f/3.0f * (field[getIndex(N+1,0,N)] + field[getIndex(N+1,1,N+1)] + field[getIndex(N,0,N+1)]);
    field[getIndex(0,N+1,N+1)] = 1.0f/3.0f * (field[getIndex(0,N+1,N)] + field[getIndex(0,N,N+1)] + field[getIndex(1,N+1,N+1)]);
    field[getIndex(N+1,N+1,N+1)] = 1.0f/3.0f * (field[getIndex(N+1,N+1,N)] + field[getIndex(N+1,N,N+1)] + field[getIndex(N,N+1,N+1)]);
}

glm::vec3 ParticleEngine::interpolateVelocity(glm::vec3 gridPos) {
    float i = glm::floor(gridPos.x);
    float j = glm::floor(gridPos.y);
    float k = glm::floor(gridPos.z);
    float xd = (gridPos.x - i);
    float yd = (gridPos.y - j);
    float zd = (gridPos.z - k);

    glm::vec3 c00 = velocityField[getIndex(i,j,k)] * (1-xd) + velocityField[getIndex(i+1,j,k)] * xd;
    glm::vec3 c01 = velocityField[getIndex(i,j,k+1)] * (1-xd) + velocityField[getIndex(i+1,j,k+1)] * xd;
    glm::vec3 c10 = velocityField[getIndex(i,j+1,k)] * (1-xd) + velocityField[getIndex(i+1,j+1,k)] * xd;
    glm::vec3 c11 = velocityField[getIndex(i,j+1,k+1)] * (1-xd) + velocityField[getIndex(i+1,j+1,k+1)] * xd;

    glm::vec3 c0 = c00 * (1-yd) + c10 * yd;
    glm::vec3 c1 = c01 * (1-yd) + c11 * yd;

    glm::vec3 c = c0 * (1-zd) + c1*zd;
    return c;
}

void ParticleEngine::updateScalarFieldBoundary(std::vector<float> &vector) {
    for (int i = 1; i <= N; ++i) {
        for (int j = 1; j <= N; ++j) {
            vector[getIndex(0,i,j)] = vector[getIndex(1,i,j)];
            vector[getIndex(N+1,i,j)] = vector[getIndex(N,i,j)];
            vector[getIndex(i,0,j)] = vector[getIndex(i,1,j)];
            vector[getIndex(i,N+1,j)] = vector[getIndex(i,N,j)];
            vector[getIndex(i,j,0)] = vector[getIndex(i,j,1)];
            vector[getIndex(i,j,N+1)] = vector[getIndex(i,j,N)];
        }
    }

    for (int i =1; i<=N; ++i) {
        vector[getIndex(0,0,i)] = 0.5f * (vector[getIndex(0,1,i)] + vector[getIndex(1,0,i)]);
        vector[getIndex(0,i,0)] = 0.5f * (vector[getIndex(0,i,1)] + vector[getIndex(1,i,0)]);
        vector[getIndex(i,0,0)] = 0.5f * (vector[getIndex(i,1,0)] + vector[getIndex(i,0,1)]);
        vector[getIndex(N+1,N+1,i)] = 0.5f * (vector[getIndex(N+1,N,i)] + vector[getIndex(N,N+1,i)]);
        vector[getIndex(N+1,i,N+1)] = 0.5f * (vector[getIndex(N+1,i,N)] + vector[getIndex(N,i,N+1)]);
        vector[getIndex(i,N+1,N+1)] = 0.5f * (vector[getIndex(i,N,N+1)] + vector[getIndex(i,N+1,N)]);
        vector[getIndex(N+1,0,i)] = 0.5f * (vector[getIndex(N+1,1,i)] + vector[getIndex(N,0,i)]);
        vector[getIndex(0,N+1,i)] = 0.5f * (vector[getIndex(0,N,i)] + vector[getIndex(1,N+1,i)]);
        vector[getIndex(N+1,i,0)] = 0.5f * (vector[getIndex(N+1,i,1)] + vector[getIndex(N,i,0)]);
        vector[getIndex(0,i,N+1)] = 0.5f * (vector[getIndex(0,i,N)] + vector[getIndex(1,i,N+1)]);
        vector[getIndex(i,N+1,0)] = 0.5f * (vector[getIndex(i,N,0)] + vector[getIndex(i,N+1,1)]);
        vector[getIndex(i,0,N+1)] = 0.5f * (vector[getIndex(i,1,N+1)] + vector[getIndex(i,0,N)]);
    }

    vector[getIndex(0,0,0)] = 1.0f/3.0f * (vector[getIndex(0,0,1)] + vector[getIndex(0,1,0)] + vector[getIndex(1,0,0)]);
    vector[getIndex(0,0,N+1)] = 1.0f/3.0f * (vector[getIndex(0,0,N)] + vector[getIndex(0,1,N+1)] + vector[getIndex(1,0,N+1)]);
    vector[getIndex(0,N+1,0)] = 1.0f/3.0f * (vector[getIndex(0,N+1,1)] + vector[getIndex(0,N,0)] + vector[getIndex(1,N+1,0)]);
    vector[getIndex(N+1,0,0)] = 1.0f/3.0f * (vector[getIndex(N+1,0,1)] + vector[getIndex(N+1,1,0)] + vector[getIndex(N,0,0)]);
    vector[getIndex(N+1,0,N+1)] = 1.0f/3.0f * (vector[getIndex(N+1,0,N)] + vector[getIndex(N+1,1,N+1)] + vector[getIndex(N,0,N+1)]);
    vector[getIndex(0,N+1,N+1)] = 1.0f/3.0f * (vector[getIndex(0,N+1,N)] + vector[getIndex(0,N,N+1)] + vector[getIndex(1,N+1,N+1)]);
    vector[getIndex(N+1,N+1,N+1)] = 1.0f/3.0f * (vector[getIndex(N+1,N+1,N)] + vector[getIndex(N+1,N,N+1)] + vector[getIndex(N,N+1,N+1)]);
}

float ParticleEngine::interpolateScalar(glm::vec3 gridPos, const std::vector<float>& field) {
    float i = glm::floor(gridPos.x);
    float j = glm::floor(gridPos.y);
    float k = glm::floor(gridPos.z);
    float xd = (gridPos.x - i);
    float yd = (gridPos.y - j);
    float zd = (gridPos.z - k);

    float c00 = field[getIndex(i,j,k)] * (1-xd) + field[getIndex(i+1,j,k)] * xd;
    float c01 = field[getIndex(i,j,k+1)] * (1-xd) + field[getIndex(i+1,j,k+1)] * xd;
    float c10 = field[getIndex(i,j+1,k)] * (1-xd) + field[getIndex(i+1,j+1,k)] * xd;
    float c11 = field[getIndex(i,j+1,k+1)] * (1-xd) + field[getIndex(i+1,j+1,k+1)] * xd;

    float c0 = c00 * (1-yd) + c10 * yd;
    float c1 = c01 * (1-yd) + c11 * yd;

    float c = c0 * (1-zd) + c1*zd;
    return c;
}
