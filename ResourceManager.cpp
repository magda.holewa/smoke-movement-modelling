//
// Created by magda on 04.12.18.
//

#include "ResourceManager.h"

std::map<std::string, Shader> ResourceManager::shaders;
std::map<std::string, Texture> ResourceManager::textures;

void
ResourceManager::LoadShader(const char *vertexShaderFilePath, const char *fragmentShaderFilePath, std::string name) {
    if(shaders.find(name) != shaders.end())
        return;
    shaders.emplace(std::pair<std::string, Shader>(name, Shader(vertexShaderFilePath, fragmentShaderFilePath)));
}

Shader ResourceManager::GetShader(std::string name) {
    return shaders.at(name);
}

void ResourceManager::LoadTexture(const char *textureFilePath, std::string name) {
    if(textures.find(name) != textures.end())
        return;
    textures.emplace(std::pair<std::string, Texture>(name, Texture(textureFilePath)));
}

Texture ResourceManager::GetTexture(std::string name) {
    return textures.at(name);
}
